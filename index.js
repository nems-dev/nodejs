import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import connectMongoDB from './config/mongodb.js';

const app = express();
const PORT = 8080;
const baseURL = '/api/v1';

dotenv.config();

connectMongoDB();

app.use(cors());
app.use(express.json());
app.use(`${baseURL}/accounts`, accounts);
app.listen(PORT,() => console.log(`Server is listening to port ${PORT}`));
